FROM python:3.8-slim

WORKDIR /app

COPY . /app

COPY response.json /app/test/response.json

RUN pip install --no-cache-dir Flask

EXPOSE 8088

CMD ["python", "app.py"]

